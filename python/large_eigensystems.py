import os
import numpy as np
import scipy
import scipy.spatial
import scipy.sparse
import scipy.sparse.linalg

from time import time

class KernelMatrixStored(scipy.sparse.linalg.LinearOperator):
    """
    Represents a linear operator (a kernel) that is stored in a number of files on disk.
    This way, the RAM requirements for kernel computations (mat/vec multiplication, eigensystems)
    can be made arbitrarily small.
    """
    
    def __init__(self, data,has_kernel_stored=False, n_kernel_normalizations=0,\
                 kernelstore_filename='kernel', verbose=False, epsilon = 1, row_steps = 100):
        self.data = data
        self.shape = (data.shape[0], data.shape[0])
        self.dtype = data.dtype
        self.epsilon = epsilon
        self.n_kernel_normalizations = n_kernel_normalizations
        
        tol = 1e-6
        self.cut_off = epsilon * np.sqrt(-np.log(tol))
        if verbose:
            print('cut_off', self.cut_off, 'at epsilon', self.epsilon,)
            
        self.row_steps = row_steps
        self.kernelstore_filename = kernelstore_filename
        
        if verbose:
            print('creating kdtree... ', end="")
        self._kdtree = scipy.spatial.cKDTree(data)
        if verbose:
            print('done.')
        
        # write kernel to file
        self.has_kernel_stored = has_kernel_stored
        if not has_kernel_stored:
            if verbose:
                print('creating kernel files... ', end="")
            self._kernel()
            
            if self.n_kernel_normalizations > 0:
                " normalize a bunch of times "
                if verbose:
                    print("normalizing kernel... ", end="")
                for k in range(self.n_kernel_normalizations):
                    if verbose:
                        print(k+1, end=" ")
                    self._normalize_kernel_files()
            if verbose:
                print('done.')
            
            # keep the matrix in memory if it is small enough
            if self.shape[0] == self.row_steps:
                self._k = self._kernel()
            else:
                self._k = None
        
    def _matvec(self, v):
        if not(self._k is None):
            return self._k @ v
        
        if len(v.shape)==1:
            v = v.reshape(-1,1)
        
        result = np.zeros((self.shape[0], v.shape[1]))
        
        for row in range(0, self.shape[0], self.row_steps):
            indices = np.arange(row, min(self.shape[0],row+self.row_steps))
            kr = self._kernelrow(indices)
            result[indices,:] = kr @ v
        if v.shape[1]==1:
            return result.reshape(-1,)
        else:
            return result
    
    def _kernelrow(self, rows):
        """
        Recomputes a given row of the kernel if self.has_kernel_stored is False.
        Otherwise, reads the rows from the file.
        """
        if not self.has_kernel_stored:
            _k = self._dist(self.data[rows,:]) / self.epsilon
            _k = _k.power(2)
            _k.data = np.exp(-_k.data)
        else:
            _k = scipy.sparse.load_npz(self._file_at_row(rows[0]))
        return (_k)
    
    def _file_at_row(self,row): return self.kernelstore_filename + "_" + str(row) + ".npz"
    
    def _normalize_kernel_files(self):
        """ normalize the kernel once, from left an right, by going through all the files """
        if self.n_kernel_normalizations == 0:
            print("Self.normalize_kernel = False. Aborting.")
            return
        _n = np.zeros(self.shape[0])
        
        # construct normalizer
        for row in range(0, self.shape[0], self.row_steps):
            indices = np.arange(row, min(self.shape[0],row+self.row_steps))
            kr = self._kernelrow(indices)
            _n[indices] = 1/np.sqrt(kr.sum(axis=1)).ravel()
        
        # the right normalizer is always the same
        _nright = scipy.sparse.diags(_n[0:self.shape[1]], 0, (self.shape[1], self.shape[1]))
        
        # right and left normalize by going through all the files again
        for row in range(0, self.shape[0], self.row_steps):
            indices = np.arange(row, min(self.shape[0],row+self.row_steps))
            kr = self._kernelrow(indices)
            _nleft = scipy.sparse.diags(_n[indices], 0, (kr.shape[0], kr.shape[0]))
            scipy.sparse.save_npz(self._file_at_row(row), _nleft @ kr @ _nright)

    def _kernel(self):
        if self.has_kernel_stored:
            """ this is merely for testing purposes. loads the entire kernel in a dense matrix. """
            _k = scipy.sparse.lil_matrix(self.shape)
            for row in range(0, self.shape[0], self.row_steps):
                _k[row:row+self.row_steps,:] = scipy.sparse.load_npz(self._file_at_row(row)).todense()
            return scipy.sparse.csr_matrix(_k)
        else:
            """ this is to initialize the kernel files. """
            for row in range(0, self.shape[0], self.row_steps):
                indices = np.arange(row, min(self.shape[0],row+self.row_steps))
                _k = self._kernelrow(indices)
                scipy.sparse.save_npz(self._file_at_row(row), _k)
                
            self.has_kernel_stored = True
            return None
    
    def _dist(self, data2):
        _kdtree2 = scipy.spatial.cKDTree(data2)
        _d = _kdtree2.sparse_distance_matrix(self._kdtree, max_distance = self.cut_off)
        return _d
    
    def _clear(self):
        for row in range(0, self.shape[0], self.row_steps):
            file = self._file_at_row(row)
            os.remove(file)



class CommonEigensystemMatrix(scipy.sparse.linalg.LinearOperator):
    """
    Represents the common eigensystem between two large kernel matrices.
    """
    
    def __init__(self, _k1, _k2, verbose=False, kernel_evecs=100, kernel_tol=1e-15, evec_tol = 1e-6, copy=False,):
        """
        _k1, _k2: linear operators, kernel matrices representing the sensor data
        verbose: will print status if True
        kernel_evecs: maximum of kernel eigenvectors to compute. Default 100
        kernel_tol: minimum eigenvalue of the kernels to use, to compute the number of eigenvectors (max: kernel_evecs). Default 1e-15.
        evec_tol: tolerance for eigensolvers. Default 1e-6
        """
        
        self.kernel_tol = kernel_tol
        self.evec_tol = evec_tol
        self.verbose = verbose
        self.kernel_evecs = kernel_evecs
        self.time_data = dict()
        
        if not copy:
            self._compute_kernel_systems(_k1, _k2)
            self.dtype = _k1.dtype
            self.shape = self.rows.shape
            self.is_adjoint = False
        
    def copy(self, deep=False):
        CEclone = CommonEigensystemMatrix(_k1=None, _k2=None, verbose=self.verbose,\
                                         kernel_evecs = self.kernel_evecs, kernel_tol=self.kernel_tol,\
                                         evec_tol=self.evec_tol, copy=True)
        CEclone.shape = self.shape
        CEclone.dtype = self.dtype
        if deep:
            CEclone.rows = self.rows.copy()
        else:
            CEclone.rows = self.rows
        return CEclone
    
    def _compute_kernel_systems(self, _k1, _k2):
        
        # use random initial state
        _rng = np.random.default_rng(1)
        v0 = _rng.normal(size=(_k1.shape[0],))
        
        if self.verbose:
            print('computing first eigensystem... ', end="")
        
        # solve both kernel problems
        n_evecs = np.min([self.kernel_evecs, _k1.shape[0]])
        
        t0 = time()
        Aevals,Aevecs = scipy.sparse.linalg.eigsh(_k1, k=n_evecs, v0=v0, tol=self.evec_tol, which="LM")
        
        self.time_data["time_eig_k1"] = time()-t0
        
        Aevals,Aevecs = self._sort_eigensystem(Aevals, Aevecs)
        Aevecs = Aevecs[:,Aevals > self.kernel_tol]
        Aevals = Aevals[Aevals > self.kernel_tol]
        if self.verbose:
            print('done')
            print('computing second eigensystem... ', end="")
        
        t0 = time()
        Bevals,Bevecs = scipy.sparse.linalg.eigsh(_k2, k=n_evecs, v0=v0,  tol=self.evec_tol, which="LM")
        
        self.time_data["time_eig_k2"] = time()-t0
        
        Bevals,Bevecs = self._sort_eigensystem(Bevals, Bevecs)
        Bevecs = Bevecs[:,Bevals > self.kernel_tol]
        Bevals = Bevals[Bevals > self.kernel_tol]
        
        if self.verbose:
            print('done')
            
        self.svd_matrix = Aevecs.T @ Bevecs
        
        n_kernels = 2 # only two up to now
        self.rows = np.column_stack([Aevecs, Bevecs]) / np.sqrt(n_kernels)
        
    def _sort_eigensystem(self, evals,evecs):
        idx = np.argsort(np.abs(evals))[::-1]
        evals = evals[idx]
        evecs = evecs[:,idx]
        return evals,evecs
    
    def _matvec(self, v):
        if self.is_adjoint:
            return self.rows.T @ v
        else:
            return self.rows @ v
        
    def _adjoint(self):
        adj = self.copy()
        adj.shape = (adj.shape[1], adj.shape[0])
        adj.is_adjoint = True
        return adj
    